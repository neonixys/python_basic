def least_division(n):
    i = 2
    while n % i != 0:
        i += 1
    return i


number = int(input("Введите число: "))
print(f"Наименьший делитель, отличный от единицы: {least_division(number)}")
