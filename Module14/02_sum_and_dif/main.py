def sum_of_digital(num):
    sum_n = 0
    while num:
        sum_n += num % 10
        num //= 10
    return sum_n


def quantity_of_digital(num):
    quantity_n = 0
    while num:
        num % 10
        quantity_n += 1
        num //= 10
    return quantity_n


number = int(input("Введите число: "))
print(f"Сумма чисел: {sum_of_digital(number)}")
print(f"Количество цифр в числе: {quantity_of_digital(number)}")
print(f"Разность суммы и количества цифр: {sum_of_digital(number) - quantity_of_digital(number)}")
